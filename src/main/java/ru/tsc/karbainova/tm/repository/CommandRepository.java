package ru.tsc.karbainova.tm.repository;

import ru.tsc.karbainova.tm.api.repository.ICommandRepository;
import ru.tsc.karbainova.tm.constant.ArgumentConst;
import ru.tsc.karbainova.tm.constant.TerminalConst;
import ru.tsc.karbainova.tm.model.Command;

public class CommandRepository implements ICommandRepository {
    public static Command EXIT = new Command(
            TerminalConst.CMD_EXIT, null, "Exit program."
    );
    public static Command HELP = new Command(
            TerminalConst.CMD_HELP, ArgumentConst.HELP, "Display list of terminal commands."
    );
    public static Command VERSION = new Command(
            TerminalConst.CMD_VERSION, ArgumentConst.VERSION, "Display program version."
    );
    public static Command INFO = new Command(
            TerminalConst.CMD_INFO, ArgumentConst.INFO, "Display info."
    );
    public static Command COMMANDS = new Command(
            TerminalConst.CMD_COMMANDS, ArgumentConst.COMMANDS, "Display commands."
    );
    public static Command ARGUMENTS = new Command(
            TerminalConst.CMD_ARGUMENTS, ArgumentConst.ARGUMENTS, "Display arguments."
    );
    public static Command ABOUT = new Command(
            TerminalConst.CMD_ABOUT, ArgumentConst.ABOUT, "Display developer info."
    );
    public static Command CREATE_PROJECT = new Command(
            TerminalConst.CREATE_PROJECT, null, "Create project."
    );
    public static Command LIST_PROJECT = new Command(
            TerminalConst.LIST_PROJECT, null, "List project."
    );
    public static Command CLEAR_PROJECT = new Command(
            TerminalConst.CLEAR_PROJECT, null, "Clear project."
    );
    public static Command CREATE_TASK = new Command(
            TerminalConst.CREATE_TASK, null, "Create task."
    );
    public static Command LIST_TASK = new Command(
            TerminalConst.LIST_TASK, null, "List task."
    );
    public static Command CLEAR_TASK = new Command(
            TerminalConst.CLEAR_TASK, null, "Clear task."
    );
    public static Command FIND_BY_ID_PROJECT = new Command(
            TerminalConst.FIND_BY_ID_PROJECT, null, "find project by id."
    );
    public static Command FIND_BY_INDEX_PROJECT = new Command(
            TerminalConst.FIND_BY_INDEX_PROJECT, null, "find project by index."
    );
    public static Command FIND_BY_NAME_PROJECT = new Command(
            TerminalConst.FIND_BY_NAME_PROJECT, null, "find project by name."
    );
    public static Command REMOVE_BY_INDEX_PROJECT = new Command(
            TerminalConst.REMOVE_BY_INDEX_PROJECT, null, "remove project by index."
    );
    public static Command REMOVE_BY_ID_PROJECT = new Command(
            TerminalConst.REMOVE_BY_ID_PROJECT, null, "remove project by id."
    );
    public static Command REMOVE_BY_NAME_PROJECT = new Command(
            TerminalConst.REMOVE_BY_NAME_PROJECT, null, "remove project by name."
    );
    public static Command UPDATE_BY_INDEX_PROJECT = new Command(
            TerminalConst.UPDATE_BY_INDEX_PROJECT, null, "update project by index."
    );
    public static Command UPDATE_BY_ID_PROJECT = new Command(
            TerminalConst.UPDATE_BY_ID_PROJECT, null, "update project by id."
    );
    public static Command FIND_BY_ID_TASK = new Command(
            TerminalConst.FIND_BY_ID_TASK, null, "find task by id."
    );
    public static Command FIND_BY_INDEX_TASK = new Command(
            TerminalConst.FIND_BY_INDEX_TASK, null, "find task by index."
    );
    public static Command FIND_BY_NAME_TASK = new Command(
            TerminalConst.FIND_BY_NAME_TASK, null, "find task by name."
    );
    public static Command REMOVE_BY_INDEX_TASK = new Command(
            TerminalConst.REMOVE_BY_INDEX_TASK, null, "remove task by index."
    );
    public static Command REMOVE_BY_ID_TASK = new Command(
            TerminalConst.REMOVE_BY_ID_TASK, null, "remove task by id."
    );
    public static Command REMOVE_BY_NAME_TASK = new Command(
            TerminalConst.REMOVE_BY_NAME_TASK, null, "remove task by name."
    );
    public static Command UPDATE_BY_INDEX_TASK = new Command(
            TerminalConst.UPDATE_BY_INDEX_TASK, null, "update task by index."
    );
    public static Command UPDATE_BY_ID_TASK = new Command(
            TerminalConst.UPDATE_BY_ID_TASK, null, "update task by id."
    );
    public static Command STATUS_START_BY_ID_PROJECT = new Command(
            TerminalConst.STATUS_START_BY_ID_PROJECT, null, "status start project by id."
    );
    public static Command STATUS_START_BY_INDEX_PROJECT = new Command(
            TerminalConst.STATUS_START_BY_INDEX_PROJECT, null, "status start project by index."
    );
    public static Command STATUS_START_BY_NAME_PROJECT = new Command(
            TerminalConst.STATUS_START_BY_NAME_PROJECT, null, "status start project by name."
    );
    public static Command STATUS_FINISH_BY_ID_PROJECT = new Command(
            TerminalConst.STATUS_FINISH_BY_ID_PROJECT, null, "status finish project by id."
    );
    public static Command STATUS_FINISH_BY_INDEX_PROJECT = new Command(
            TerminalConst.STATUS_FINISH_BY_INDEX_PROJECT, null, "status finish project by index."
    );
    public static Command STATUS_FINISH_BY_NAME_PROJECT = new Command(
            TerminalConst.STATUS_FINISH_BY_NAME_PROJECT, null, "status finish project by name."
    );
    public static Command STATUS_START_BY_ID_TASK = new Command(
            TerminalConst.STATUS_START_BY_ID_TASK, null, "status start task by id."
    );
    public static Command STATUS_START_BY_INDEX_TASK = new Command(
            TerminalConst.STATUS_START_BY_INDEX_TASK, null, "status start task by index."
    );
    public static Command STATUS_START_BY_NAME_TASK = new Command(
            TerminalConst.STATUS_START_BY_NAME_TASK, null, "status start task by name."
    );
    public static Command STATUS_FINISH_BY_ID_TASK = new Command(
            TerminalConst.STATUS_FINISH_BY_ID_TASK, null, "status finish task by id."
    );
    public static Command STATUS_FINISH_BY_INDEX_TASK = new Command(
            TerminalConst.STATUS_FINISH_BY_INDEX_TASK, null, "status finish task by index."
    );
    public static Command STATUS_FINISH_BY_NAME_TASK = new Command(
            TerminalConst.STATUS_FINISH_BY_NAME_TASK, null, "status finish task by name."
    );

    public static final Command[] TERMINAL_COMMANDS = new Command[]{
            EXIT, HELP, VERSION, INFO, ABOUT, ARGUMENTS, COMMANDS,
            CLEAR_PROJECT, CREATE_PROJECT, LIST_PROJECT,
            CLEAR_TASK, CREATE_TASK, LIST_TASK,
            UPDATE_BY_ID_PROJECT, UPDATE_BY_INDEX_PROJECT,
            REMOVE_BY_ID_PROJECT, REMOVE_BY_INDEX_PROJECT, REMOVE_BY_NAME_PROJECT,
            FIND_BY_ID_PROJECT, FIND_BY_INDEX_PROJECT, FIND_BY_NAME_PROJECT,
            UPDATE_BY_ID_TASK, UPDATE_BY_INDEX_TASK,
            REMOVE_BY_ID_TASK, REMOVE_BY_INDEX_TASK, REMOVE_BY_NAME_TASK,
            FIND_BY_ID_TASK, FIND_BY_INDEX_TASK, FIND_BY_NAME_TASK,
            STATUS_FINISH_BY_ID_PROJECT, STATUS_FINISH_BY_INDEX_PROJECT, STATUS_FINISH_BY_NAME_PROJECT,
            STATUS_START_BY_ID_PROJECT, STATUS_START_BY_INDEX_PROJECT, STATUS_START_BY_NAME_PROJECT,
            STATUS_FINISH_BY_ID_TASK, STATUS_FINISH_BY_INDEX_TASK, STATUS_FINISH_BY_NAME_TASK,
            STATUS_START_BY_ID_TASK, STATUS_START_BY_INDEX_TASK, STATUS_START_BY_NAME_TASK
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }
}
